﻿using System.Threading;
using RabbitMQ.Wrapper;

namespace bsa_rabbit
{
    class Pinger
    {
        static void Main(string[] args)
        {

            Wrapper.SendMessageToQueue("Ping", "pong_queue");
            Wrapper.ListenQueue("ping_queue", SendMessage);
        }

        static void SendMessage(int delay)
        {
            Thread.Sleep(2500);
            Wrapper.SendMessageToQueue("Ping", "pong_queue");
        }
    }
}
