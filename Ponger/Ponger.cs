﻿using System.Threading;
using RabbitMQ.Wrapper;

namespace Ponger
{
    class Ponger
    {
        static void Main(string[] args)
        {
            Wrapper.ListenQueue("pong_queue", SendMessage);
        }

        static void SendMessage(int delay)
        {
            Thread.Sleep(2500);
            Wrapper.SendMessageToQueue("Pong", "ping_queue");
        }
    }
}
